<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//buku
Route::apiResource('book', 'API\V1\BookController');

//register
Route::apiResource('visitor', 'API\V1\visitorController');

//genre
Route::apiResource('genre', 'API\V1\GenreController');

